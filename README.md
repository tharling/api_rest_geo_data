# API REST par Tristan Harling



Grâce a cette API fonctionnant sous Java (framework Spring) on va pouvoir réaliser des opérations sur des géométries. 

  - Des géométries sont proposées sur la page de calcul.
  - Libre à vous d'envoyer la votre dans l'appel à l'API REST.
  - L'interface est soignée et portée sur la visualisation.
  - Le résultat est affiché sur une carte.(*non centrée*)

# Les Opérations!

  - Calcul du double périmètre (en m.)
  - Enveloppe Convexe, *pour une couche multipoint*
  - Centroide


# How To
le programme execute un web-service (fonctionnant sous Spring).
Il faut avoir l'environnement Java J2SE-1.5(java-11-opensdk-amd64).
  - Se placer dans le terminal au dossier Projet_Jar/.
  - Lancer dans le terminal :
```sh
java -jar Api_rest_tristan-0.0.1-SNAPSHOT.jar
```
  - Lancer la page web [localhost:8080](localhost:8080).
  - Choisir la page *Calculer une opération géométrique* depuis la page d'accueil.
  - Vous pouvez choisir une des 3 géométries depuis la carte, son geojson correspondant s'ajoutera automatiquement dans le champ *Choisir directement mon JSON*, ou vous pouvez écrire le votre !
  - Choisir le style d'opération.
  - Cliquer sur Submit.
  


# Tech

l'API REST fonctionne sur des Requetes POST et GET envoyées au navigateur:

POST: 
![alt text](images/POST.png "POST")
c'est dans le post que la geometrie json est envoyee, mais aussi un argument qui indique la geometrie, qui dirigera la requete vers la bonne fonction dans le code JAVA.



GET:
![alt text](images/GET.png "GET")
C'est après avoir effectuer le traitement géométrique que la donnée est renvoyée vers le client, qui reçoit un GET.



Voici les librairies que j'ai utilisées

* [Spring] - Framework pour créer des web-service en MVC!
* [Thymleaf] - affichage de page HTML en dynamic, assurant des échanges vers Spring.
* [Geotools] - Librairie complete pour le traitement d'objets géographiques.
* [Swagguer UI] - Permet de créer de la doc pour aider l'utilisateur avec l'API proposée, devient très utile lors de gros projets !.
* [Leaflet] - une API de cartographie pour le web assez complete, pouvant lire du GeoJSON.



# Rapport sur le projet

Concernant les attentes du projets, je pense que le fonctionnement *POST/GET* pour envoyer/recevoir les requetes est réussi mais il y a malheureusement pas le fonctionnement classique d'une API par requete faite "dans la barre d'adresse".
Celà fait suite à l'implémentation d'une interface graphique et dynamique.
J'ai choisis Thymeleaf pour récuperer le texte venant de balises html (balises input) et l'envoyer ensuite directement à Spring, dans le *controller*.
On a donc plus à entrer un texte geojson dans la barre d'adresse. Celà est plus esthétique et innovant.

Il a fallu être malin: il y a une balise qui attend du GeoJSON au format String et une autre qui attend le type d'opération (au format String), cette dernière étant limitée à 3 opérations, j'ai créer 3 radio buttons. ce choix se justifie car les radios buttons ne peuvent envoyer que des valeurs finies.(*centroide*, *convexe_hull* ou *double_perimetre*).
Le fonctionnement de Spring veut qu'il y ai une classe implémentée asosociée pour instancier les valeurs rentrées dans les balises dans un objet Java.
C'est la classe Myjson.java, qui contient donc :

 - **id** : le nom de l'opération géométrique.
 - **json_as_txt** : le string correspondant au GeoJSON.

Concernant la partie front end, j'ai utilisé Leaflet.
On peut selectionner des géométries proposées (du code javascript, qui detecte les cliques). A ce moment la le pop up portant le nom de la géométrie apparait, et la balise correspondant au format GeoJSON se remplie automatiquement. 

La géométrie renvoyée en réponse sur une nouvelle page doit aussi être affichée dans leaflet. il faut donc insérer la partie **json_as_txt** dans du code javascript. celà est fait assez simplement toujours grâce à Thymeleaf. la grammaire commence à être compliquée:
*JSON.parse(/\*[[${myjson.json_as_txt}]]\*/)* lorsqu'on déclare le GeoJSON dans leaflet. 
j'ai décidé pour plus d'intéraction d'ajouter un pop up nommant l'opération géométrique **id** pour le rappeler à l'utilisateur. Il est lié à la figure apparaissant sur la carte. Ex: 
![alt text](images/reponse.png "résultat")

Comme la fonction *double_perimetre* renvoie un string et non une géométrie, la carte ne peut se charger (on peut le voir dans la console). On a donc un simple texte avec la valeur en mètres.

# Tests

Mes tests sont effectués avec Junit. Il faut juste se soucier de tous les type d'entrée possible et créer des cas pour chaque. J'ai donc une fonction *MyjsonDaoImplTest* qui execute 7 test, couvrant tous les cas possibles. J'ai un coverage à la fin de 78%.



**2018 - ENSG - Projet Perso 1 - Harling Tristan**
