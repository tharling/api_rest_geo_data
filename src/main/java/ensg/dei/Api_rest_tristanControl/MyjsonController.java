package ensg.dei.Api_rest_tristanControl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;


@Api( description="API pour faire des opértions géometriques.")
@Controller
public class MyjsonController {
//	
//
//	
	@Autowired
    private MyjsonDao myjsondao;
	


	    //Récupérer un JSON
	    @ApiOperation(value = "Permet sur un JSON que l'on veux de faire 3 types d'opérations entre : Double Périmetre, Enveloppe Convexe ou encore Centroid")
	    @GetMapping("/myjson")
	    public String myjsonForm(Model model) {
	    	model.addAttribute("myjson",new Myjson());
	        return "myjson";
	    }
	    
	    @PostMapping("/myjson")
	    public String myjsonSubmit(@ModelAttribute Myjson myjson) {
	    	String new_json=myjsondao.choix_compute(myjson.getId(),myjson.getJson_as_txt());
	    	myjson.setJson_as_txt(new_json);
	    	return "service";
	    	
	    	
	}
}
