package ensg.dei.Api_rest_tristanControl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.filter.AreaFunction;
import org.geotools.geojson.feature.FeatureJSON;
import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.Geometries;
import org.geotools.geometry.jts.GeometryCollector;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.GeometryCollection;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.geometry.MismatchedDimensionException;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPoint;


public class GeotoolsFunction {
	
	public static String double_perimetre(FeatureIterator<SimpleFeature> Collect_obj){
		SimpleFeature feature_obj;
		double perim_tot=0;
		while(Collect_obj.hasNext()) {

		   feature_obj = Collect_obj.next();
		   Geometry g = (Geometry) feature_obj.getAttribute( 0 );
		   System.out.println(g.toText());
		   //passage en coordoonées metriques
		   CoordinateReferenceSystem crs;
		   CoordinateReferenceSystem crs_fin;
		   try {
			   String wkt = "GEOGCS[" + "\"WGS 84\"," + "  DATUM[" + "    \"WGS_1984\","
				        + "    SPHEROID[\"WGS 84\",6378137,298.257223563,AUTHORITY[\"EPSG\",\"7030\"]],"
				        + "    TOWGS84[0,0,0,0,0,0,0]," + "    AUTHORITY[\"EPSG\",\"6326\"]],"
				        + "  PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],"
				        + "  UNIT[\"DMSH\",0.0174532925199433,AUTHORITY[\"EPSG\",\"9108\"]],"
				        + "  AXIS[\"Lat\",NORTH]," + "  AXIS[\"Long\",EAST],"
				        + "  AUTHORITY[\"EPSG\",\"4326\"]]";

			   crs = CRS.parseWKT(wkt);
			   String wkt2="PROJCS[\"RGF93 Lambert-93\",GEOGCS[\"RGF93\",DATUM[\"Reseau_Geodesique_Francais_1993\",SPHEROID[\"GRS 1980\",6378137,298.257222101,AUTHORITY[\"EPSG\",\"7019\"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY[\"EPSG\",\"6171\"]],PRIMEM[\"Greenwich\",0,AUTHORITY[\"EPSG\",\"8901\"]],UNIT[\"degree\",0.01745329251994328,AUTHORITY[\"EPSG\",\"9122\"]],AUTHORITY[\"EPSG\",\"4171\"]],UNIT[\"metre\",1,AUTHORITY[\"EPSG\",\"9001\"]],PROJECTION[\"Lambert_Conformal_Conic_2SP\"],PARAMETER[\"standard_parallel_1\",49],PARAMETER[\"standard_parallel_2\",44],PARAMETER[\"latitude_of_origin\",46.5],PARAMETER[\"central_meridian\",3],PARAMETER[\"false_easting\",700000],PARAMETER[\"false_northing\",6600000],AUTHORITY[\"EPSG\",\"2154\"],AXIS[\"X\",EAST],AXIS[\"Y\",NORTH]]";
			   crs_fin = CRS.parseWKT(wkt2);
		   } catch (NoSuchAuthorityCodeException e) {
			   // TODO Auto-generated catch block
			   System.out.println("non");
			   e.printStackTrace();
			   return null;
		   } catch (FactoryException e) {
			   // TODO Auto-generated catch block
			   System.out.println("non");
			   e.printStackTrace();
			   return null;
		   }
		   System.out.println("eeeeeeeeeeeee");
		   MathTransform transform;
		   try {
			   transform = CRS.findMathTransform(crs, crs_fin, false);
		   } catch (FactoryException e) {
			// TODO Auto-generated catch block
			   System.out.println("noon");
			   e.printStackTrace();

			   return null;
		   }
		   Geometry g_lambert;
		   try {
			   g_lambert = JTS.transform(g, transform);
		   } catch (MismatchedDimensionException e) {
			   // TODO Auto-generated catch block
			   System.out.println("nooooon");
			   e.printStackTrace();

			   return null;
		   } catch (TransformException e) {
			   // TODO Auto-generated catch block
			   System.out.println("noooooooooon");
			   e.printStackTrace();

			   return null;
		   }
		   AreaFunction area=new AreaFunction();
		   perim_tot+=area.getPerimeter(g_lambert);

		}
		System.out.println(perim_tot);
		return Double.toString(2*perim_tot);
		
	}
	
	
	// Il faut absolument des multipoints 
	public static String Convex_hull(FeatureIterator<SimpleFeature> Collect_obj){
		SimpleFeature feature_obj;
		
		GeometryCollector global_geometries=new GeometryCollector();
		while(Collect_obj.hasNext()) {
			
		    Geometry g_out;
		    feature_obj = Collect_obj.next();
		    Geometry g = (Geometry) feature_obj.getAttribute( 0 );
		    //verirife si multipoint
		    Geometries geomType = Geometries.get(g);
		    switch (geomType) {
		    	case MULTIPOINT:
		    		System.out.println(geomType);
		    		g_out=g.convexHull();
		    		global_geometries.add(g_out);
		    		
		    		break;
		    	default:
		    		System.out.println("erreur veuillez prendre un multipoint");
		    		return "Veuillez prendre un multi point";
		    }
		}

		com.vividsolutions.jts.geom.GeometryCollection g_final=global_geometries.collect();
		StringWriter message = new StringWriter();
		GeometryJSON my_json=new GeometryJSON();
		try {
			my_json.writeGeometryCollection(g_final,message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			return message.toString();
		
		
		
	}
	
//	
//	
	public static String centroid(FeatureIterator<SimpleFeature> Collect_obj) {
		SimpleFeature feature_obj;
		GeometryCollector global_geometries=new GeometryCollector();
		while(Collect_obj.hasNext()) {
		    Geometry g_out;
		    feature_obj = Collect_obj.next();
		    Geometry g = (Geometry) feature_obj.getAttribute( 0 );

		    g_out=g.getCentroid();
		    global_geometries.add(g_out);

		}
		com.vividsolutions.jts.geom.GeometryCollection g_final=global_geometries.collect();
		StringWriter message = new StringWriter();
		GeometryJSON my_json=new GeometryJSON();
		try {
			my_json.writeGeometryCollection(g_final,message);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return message.toString();

	}
}
