package ensg.dei.Api_rest_tristanControl;

public class Validation_object {
	public static String validation_object(String id,String json_txt) {

		if(id != null && !id.isEmpty()) {
			if(json_txt != null && !json_txt.isEmpty()) {
				if(json_txt.toCharArray()[0]!='{') {
					return "Erreur vous n'avez pas de bon JSON !";
				}
				//case if : not null id and not null json_txt and valid json
				return null;
			}
			else {
				return "Vous n'avez pas sélectionné de géométrie !";
			}
			
		}
		else {
			return "Vous n'avez pas sélectionné d'opérations !";
		}
		
	}
}
