package ensg.dei.Api_rest_tristanControl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geojson.feature.FeatureJSON;
import org.opengis.feature.simple.SimpleFeature;
import org.springframework.stereotype.Service;


@Service
public class MyjsonDaoImpl implements MyjsonDao{
	

	public List<Myjson> findAll() {
		// TODO Auto-generated method stub
		return null;
	}
	public MyjsonDaoImpl() {};

	public String choix_compute(String id, String json_txt) {
		//on regarde si les champs ont bien été remplis et sont corrects
		String validation=Validation_object.validation_object(id,json_txt);
		if(validation!=null) {
			return validation;
		}
		SimpleFeature feature_obj;
		FeatureJSON io =new FeatureJSON();
		//Reader reader=new StringReader(json_txt);
		FeatureIterator<SimpleFeature> Collect_obj;
		//FeatureCollection fc;
		InputStream json_stream = new ByteArrayInputStream(json_txt.getBytes());
		try {
			Collect_obj=(io.readFeatureCollection(json_stream)).features();
		} catch (IOException e) {
			//Si on a pas un json au bon format
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
		
		//SUR L'opération 1 on fait le double périmetre
		if(id.equals("Double_Perimetre")) {
			System.out.println(json_txt);
			return GeotoolsFunction.double_perimetre(Collect_obj);
		}
		//SUR L'opération 2 on fait la convex hull
		if(id.equals("Enveloppe_Convexe")) {
			return GeotoolsFunction.Convex_hull(Collect_obj);
		}
		if(id.equals("Centroide")) {
			return GeotoolsFunction.centroid(Collect_obj);
		}
		else {
			return "";
		}
				

		
	};
	
	

}
