package ensg.dei.Api_rest_tristan;

import org.springframework.boot.WebApplicationType;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableSwagger2
@ComponentScan("ensg.dei.Api_rest_tristanControl")
public class Application
{
	public static void main(String[] args) {
		// mode standard
		//SpringApplication.run(Application.class, args);

		/*
		 * Permet d'accéder facilement aux resources en mode jar
		 * @see https://stackoverflow.com/a/37202883
		 */
		new SpringApplicationBuilder()
			.sources(Application.class,SwaggerConfig.class)
			.run(args)
		;
	}
}