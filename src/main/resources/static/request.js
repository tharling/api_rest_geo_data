//la variable finale pour le choix du json
var choix_geojson=L.geoJSON();

var myIcon2 = L.icon({
  iconUrl: 'marker-icon.png',
  iconSize: [25, 41],
  iconAnchor: [16, 37],
  popupAnchor: [0, -28]
});
var map = L.map('map',{
  closePopupOnClick: false
}).on('click', function() { this.closePopup(); }).setView([48.84, 2.652],14);

L.tileLayer('https://a.tile.openstreetmap.se/hydda/full/{z}/{x}/{y}.png',{
        attribution: 'Tiles &copy; Esri, OpenStreetMap & Ign &mdash; Source: Ign, Esri, DeLorme, NAVTEQ, USGS, Intermap'
        }).addTo(map);
var polygon1=L.geoJSON(json1).addTo(map).bindPopup("<b>Zone Pavillonaire</b>");
var polygon2 = L.geoJSON(json2).addTo(map).bindPopup("<b>Parc</b>");
var polygon3 = L.geoJSON(json3,{
  pointToLayer: function (feature, latlng) {
            return L.marker(latlng, {icon: myIcon2});
    }}).addTo(map).bindPopup("<b>Lac et Etangs</b>");


/*espace pour ajouter les effet de couleur, et la selection */
polygon1.on('popupopen', function (e) {
  e.target.setStyle({color: "#78Ea78"});
  //on a selectioné 1
  choix_geojson=polygon1;
  document.getElementById("mon_choix").value= JSON.stringify(choix_geojson.toGeoJSON());

});
polygon1.on('popupclose',function (e) {
  e.target.setStyle({color: "#3388ff"});

});
polygon2.on('popupopen', function (e) {
  e.target.setStyle({color: "#78Ea78"});
  //on a selectioné 2
  choix_geojson=polygon2;
  document.getElementById("mon_choix").value= JSON.stringify(choix_geojson.toGeoJSON());
});
polygon2.on('popupclose',function (e) {
  e.target.setStyle({color: "#3388ff"});
});
polygon3.on('popupopen', function (e) {
  e.target.setStyle({color: "#78Ea78"});
  //on a selectioné 3
  choix_geojson=polygon3;
  document.getElementById("mon_choix").value= JSON.stringify(choix_geojson.toGeoJSON());
});
polygon3.on('popupclose',function (e) {
  e.target.setStyle({color: "#3388ff"});
});
