package ensg.dei.Api_rest_tristan;




import org.junit.Assert;
import org.junit.Test;

import ensg.dei.Api_rest_tristanControl.Myjson;
import ensg.dei.Api_rest_tristanControl.MyjsonDaoImpl;

public class MyjsonDaoImplTest {
	
	
	@Test
	//test with a non json string
	public void TestMyjsonwrong() {
		MyjsonDaoImpl myjsondaowrong = new MyjsonDaoImpl();
		Myjson my_json_object= new Myjson();
		String json_id="Centroide";
		
		//test with a non json string
		String json_wrong="faux";
		
		my_json_object.setId(json_id);
		my_json_object.setJson_as_txt(json_wrong);
		Assert.assertEquals("Erreur vous n'avez pas de bon JSON !",myjsondaowrong.choix_compute(my_json_object.getId(),my_json_object.getJson_as_txt()));
	}
	
	@Test
	//test with no geometry
	//l'utilisateur n'a pas rempli le champs geometrie
	public void TestMyjson_no_json() {
		MyjsonDaoImpl myjsondaowrong = new MyjsonDaoImpl();
		Myjson my_json_object= new Myjson();
		String json_id="Centroide";
		
		//test with a null string
		String json_wrong=null;
		
		my_json_object.setId(json_id);
		my_json_object.setJson_as_txt(json_wrong);
		Assert.assertEquals("Vous n'avez pas sélectionné de géométrie !",myjsondaowrong.choix_compute(my_json_object.getId(),my_json_object.getJson_as_txt()));
	}
	
	@Test
	//test with no operation
	//l'utilisateur n'a pas rempli le champs opération
	public void TestMyjson_no_operation() {
		MyjsonDaoImpl myjsondaowrong = new MyjsonDaoImpl();
		Myjson my_json_object= new Myjson();
		String json_id_wrong=null;
		
		//test with a null string
		String json="{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[2.658606,48.833001],[2.657876,48.83399],[2.657404,48.835204],[2.656116,48.83608],[2.654958,48.835543],[2.654958,48.834074],[2.654743,48.833792],[2.653069,48.834639],[2.651782,48.833933],[2.646503,48.83286],[2.646503,48.83221],[2.652898,48.832549],[2.658606,48.833001]]]}}]}";

		
		my_json_object.setId(json_id_wrong);
		my_json_object.setJson_as_txt(json);
		Assert.assertEquals("Vous n'avez pas sélectionné d'opérations !",myjsondaowrong.choix_compute(my_json_object.getId(),my_json_object.getJson_as_txt()));
	}
	
	@Test
	//cas normal calcul centroide
	public void TestMyjsoncentroid() {
		MyjsonDaoImpl myjsondaowrong = new MyjsonDaoImpl();
		Myjson my_json_object= new Myjson();
		String json_id="Centroide";
		String json_polygon="{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[2.658606,48.833001],[2.657876,48.83399],[2.657404,48.835204],[2.656116,48.83608],[2.654958,48.835543],[2.654958,48.834074],[2.654743,48.833792],[2.653069,48.834639],[2.651782,48.833933],[2.646503,48.83286],[2.646503,48.83221],[2.652898,48.832549],[2.658606,48.833001]]]}}]}";
		my_json_object.setId(json_id);
		my_json_object.setJson_as_txt(json_polygon);
		Assert.assertEquals("{\"type\":\"GeometryCollection\",\"geometries\":[{\"type\":\"Point\",\"coordinates\":[2.6535,48.8336]}]}",myjsondaowrong.choix_compute(my_json_object.getId(),my_json_object.getJson_as_txt()));
	}
	
	@Test
	//cas normal calcul double perimetre
	public void TestMyjsonperimetre() {
		MyjsonDaoImpl myjsondaowrong = new MyjsonDaoImpl();
		Myjson my_json_object= new Myjson();
		String json_id="Double_Perimetre";
		String json_polygon="{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[2.628565,48.843396],[2.629509,48.843113],[2.630882,48.841814],[2.631526,48.841814],[2.631955,48.841277],[2.633801,48.840995],[2.636333,48.841221],[2.637663,48.841616],[2.638478,48.842548],[2.639637,48.843537],[2.641268,48.844102],[2.644401,48.843791],[2.641783,48.844921],[2.641826,48.845457],[2.640109,48.84526],[2.636161,48.845203],[2.633543,48.845542],[2.630582,48.845175],[2.628565,48.843396]]]}}]}";
		my_json_object.setId(json_id);
		my_json_object.setJson_as_txt(json_polygon);
		Assert.assertEquals("10037.517139750506",myjsondaowrong.choix_compute(my_json_object.getId(),my_json_object.getJson_as_txt()));
	}
	
	@Test
	//cas enveloppe convexe calculée a partir d'autre elements que du multipoint
	public void TestMyjson_wrong_convexe_hull() {
		MyjsonDaoImpl myjsondaowrong = new MyjsonDaoImpl();
		Myjson my_json_object= new Myjson();
		String json_id="Enveloppe_Convexe";
		
		//type is different from multipoint
		
		String json_polygon="{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"Polygon\",\"coordinates\":[[[2.628565,48.843396],[2.629509,48.843113],[2.630882,48.841814],[2.631526,48.841814],[2.631955,48.841277],[2.633801,48.840995],[2.636333,48.841221],[2.637663,48.841616],[2.638478,48.842548],[2.639637,48.843537],[2.641268,48.844102],[2.644401,48.843791],[2.641783,48.844921],[2.641826,48.845457],[2.640109,48.84526],[2.636161,48.845203],[2.633543,48.845542],[2.630582,48.845175],[2.628565,48.843396]]]}}]}";
		my_json_object.setId(json_id);
		my_json_object.setJson_as_txt(json_polygon);
		Assert.assertEquals("Veuillez prendre un multi point",myjsondaowrong.choix_compute(my_json_object.getId(),my_json_object.getJson_as_txt()));
	}
	
	//cas normal calcul enveloppe convexe
	@Test
	public void TestMyjsonconvexe_hull() {
		MyjsonDaoImpl myjsondaowrong = new MyjsonDaoImpl();
		Myjson my_json_object= new Myjson();
		String json_id="Enveloppe_Convexe";
		String json_multipoint="{\"type\":\"FeatureCollection\",\"features\":[{\"type\":\"Feature\",\"properties\":{},\"geometry\":{\"type\":\"MultiPoint\",\"coordinates\":[[2.644701,48.843424],[2.640581,48.843085],[2.638779,48.841588],[2.64071,48.84187],[2.641525,48.841955],[2.642941,48.841644],[2.64483,48.841842],[2.646418,48.841447],[2.64852,48.841249],[2.647448,48.842379],[2.646375,48.842915],[2.645473,48.842887]]}}]}";
		my_json_object.setId(json_id);
		my_json_object.setJson_as_txt(json_multipoint);
		Assert.assertEquals("{\"type\":\"GeometryCollection\",\"geometries\":[{\"type\":\"Polygon\",\"coordinates\":[[[2.6485,48.8412],[2.6388,48.8416],[2.6406,48.8431],[2.6447,48.8434],[2.6464,48.8429],[2.6474,48.8424],[2.6485,48.8412]]]}]}",myjsondaowrong.choix_compute(my_json_object.getId(),my_json_object.getJson_as_txt()));
	}


}
